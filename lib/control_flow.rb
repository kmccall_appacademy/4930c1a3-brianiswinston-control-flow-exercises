# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  alpha = ("a".."z").to_a
  str.chars.delete_if {|let| alpha.include?(let)}.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.chars.count.odd?
    mid = str.chars.count / 2
    str[mid]
  else
    mid = (str.chars.count / 2) - 1
    str[mid..mid + 1]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.chars.each {|let| count += 1 if VOWELS.include?(let)}
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  return num if num <= 2
  num * factorial(num - 1)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  answer = []
  arr.each {|let| answer << let}
  answer.join("#{separator}")
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.chars.map.with_index {|let, idx| idx.odd? ? let.upcase : let.downcase}.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str.split.map { |word| word.chars.count >= 5 ? word.reverse : word }.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  answer = []
  idx = 1
  while idx <= n
    if idx % 5 == 0 && idx % 3 == 0
      answer << "fizzbuzz"
    elsif idx % 5 == 0
      answer << "buzz"
    elsif idx % 3 == 0
      answer << "fizz"
    else
      answer << idx
    end

    idx += 1
  end
  answer
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num <= 1

  idx = 2
  while idx < num
    return false if num % idx == 0
    idx += 1
  end

  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  answer = []
  (1..num).to_a.each { |numb| answer << numb if num % numb == 0 }
  answer.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  answer = []
  holder = factors(num)
  holder.each { |fac| answer << fac if prime?(fac) }
  answer
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd = 0
  arr.each { |num| odd += 1 if num.odd? }
  if odd > 1
    arr.each { |num| return num if num.even? }
  else
    arr.each { |num| return num if num.odd? }
  end
end
